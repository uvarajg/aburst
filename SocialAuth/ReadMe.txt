===============================================================================
WELCOME to SocialAuth Android SDK version 3.0
===============================================================================

The SDK contains all the material you need to use SocialAuth Android project.
The directories are as follows:

1. libs - This contains socialauth-android-3.0.jar and socialauth-4.3.jar. You need 
to include both of these in your application. The socialauth-3.2.jar is the 
underlying OAuth infrastructure and the socialauth-android.jar has the Android 
components.

2. assets - Contains oauth_consumer.properties containing sample key and secrets
which can use for testing purposes

3. src - This contains the entire socialauth-android-3.0 Eclipse project if you
would like to debug or you need to make some changes. Most of the time you
should not need to.

4. examples - This contains four examples that we have built for you showing
how the SDK can be used to build a Share button, Share bar, Share Menu or a completely
custom UI.  

5. javadoc - Documentation for SocialAuth Android

===============================================================================
Detailed wiki available at http://code.google.com/p/socialauth-android/
===============================================================================
